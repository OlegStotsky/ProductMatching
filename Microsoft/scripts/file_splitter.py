import os
import math
import sys

NUM_LINES_PER_FILE = 1e6

if __name__ == "__main__":
	_, file_name = sys.argv
	file_name_prefix, file_name_suffix = file_name.split('.')

	if os.path.exists(file_name):
		with open(file_name, "r", encoding="utf8") as f:
			lines = f.readlines()
			num_files = math.ceil(float(len(lines)) / NUM_LINES_PER_FILE)
			print("Number of chunks that will be created: {0}".format(num_files))
			files = [open("{0}_chunk_{1}.{2}".format(file_name_prefix , i, file_name_suffix), "w", encoding="utf8") for i in range(num_files)]
			cur_file = 0
			for i, line in enumerate(lines):
				if i == NUM_LINES_PER_FILE*cur_file:
					cur_file += 1
					print("Creating chunk number: {0}".format(cur_file-1))
				files[cur_file-1].write(line)
	else:
		print("File doesn't exist")